document.addEventListener("DOMContentLoaded", () => {
    const playerOptions = document.querySelectorAll(".player img");
    const computerOptions = document.querySelectorAll(".computer img");
    const resultText = document.querySelector(".result-text");
    const resetImage = document.querySelector(".result_images");

    const choices = ["rock", "paper", "scissors"];

    function computerPlay() {
        const randomIndex = Math.floor(Math.random() * choices.length);
        return choices[randomIndex];
    }

    function determineWinner(playerChoice, computerChoice) {
        if (playerChoice === computerChoice) {
            return "Draw";
        } else if (
            (playerChoice === "rock" && computerChoice === "scissors") ||
            (playerChoice === "scissors" && computerChoice === "paper") ||
            (playerChoice === "paper" && computerChoice === "rock")
        ) {
            return "Player wins";
        } else {
            return "Computer wins";
        }
    }

    function showResult(result) {
        resultText.textContent = result;
    }

    function resetGame() {
        resultText.textContent = "";
    }

    function playerSelect() {
        const playerChoice = this.dataset.choice;
        const computerChoice = computerPlay();
        const result = determineWinner(playerChoice, computerChoice);
        showResult(result);
    }

    playerOptions.forEach((option) => {
        option.addEventListener("click", playerSelect);
    });

    resetImage.addEventListener("click", resetGame);
});